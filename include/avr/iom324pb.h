//
// Created by paulh on 5/12/2020.
//

#ifndef _AVR_ATMEGA324PB_H_INCLUDED
#define _AVR_ATMEGA324PB_H_INCLUDED

#include <stdint.h>

#define F_CPU 16000000L

// PORTS

extern volatile uint8_t virtualPINA;
#define PINA    (virtualPINA)
#define PINA7   7
#define PINA6   6
#define PINA5   5
#define PINA4   4
#define PINA3   3
#define PINA2   2
#define PINA1   1
#define PINA0   0

extern volatile uint8_t virtualDDRA;
#define DDRA    (virtualDDRA)
#define DDRA7   7
#define DDRA6   6
#define DDRA5   5
#define DDRA4   4
#define DDRA3   3
#define DDRA2   2
#define DDRA1   1
#define DDRA0   0

extern volatile uint8_t virtualPORTA;
#define PORTA   (virtualPORTA)
#define PORTA7  7
#define PORTA6  6
#define PORTA5  5
#define PORTA4  4
#define PORTA3  3
#define PORTA2  2
#define PORTA1  1
#define PORTA0  0

extern volatile uint8_t virtualPINB;
#define PINB    (virtualPINB)
#define PINB7   7
#define PINB6   6
#define PINB5   5
#define PINB4   4
#define PINB3   3
#define PINB2   2
#define PINB1   1
#define PINB0   0

extern volatile uint8_t virtualDDRB;
#define DDRB    (virtualDDRB)
#define DDRB7   7
#define DDRB6   6
#define DDRB5   5
#define DDRB4   4
#define DDRB3   3
#define DDRB2   2
#define DDRB1   1
#define DDRB0   0

extern volatile uint8_t virtualPORTB;
#define PORTB   (virtualPORTB)
#define PORTB7  7
#define PORTB6  6
#define PORTB5  5
#define PORTB4  4
#define PORTB3  3
#define PORTB2  2
#define PORTB1  1
#define PORTB0  0

extern volatile uint8_t virtualPINC;
#define PINC    (virtualPINC)
#define PINC7   7
#define PINC6   6
#define PINC5   5
#define PINC4   4
#define PINC3   3
#define PINC2   2
#define PINC1   1
#define PINC0   0

extern volatile uint8_t virtualDDRC;
#define DDRC    (virtualDDRC)
#define DDRC7   7
#define DDRC6   6
#define DDRC5   5
#define DDRC4   4
#define DDRC3   3
#define DDRC2   2
#define DDRC1   1
#define DDRC0   0

extern volatile uint8_t virtualPORTC;
#define PORTC   (virtualPORTC)
#define PORTC7  7
#define PORTC6  6
#define PORTC5  5
#define PORTC4  4
#define PORTC3  3
#define PORTC2  2
#define PORTC1  1
#define PORTC0  0

extern volatile uint8_t virtualPIND;
#define PIND    (virtualPIND)
#define PIND7   7
#define PIND6   6
#define PIND5   5
#define PIND4   4
#define PIND3   3
#define PIND2   2
#define PIND1   1
#define PIND0   0

extern volatile uint8_t virtualDDRD;
#define DDRD    (virtualDDRD)
#define DDRD7   7
#define DDRD6   6
#define DDRD5   5
#define DDRD4   4
#define DDRD3   3
#define DDRD2   2
#define DDRD1   1
#define DDRD0   0

extern volatile uint8_t virtualPORTD;
#define PORTD   (virtualPORTD)
#define PORTD7  7
#define PORTD6  6
#define PORTD5  5
#define PORTD4  4
#define PORTD3  3
#define PORTD2  2
#define PORTD1  1
#define PORTD0  0

extern volatile uint8_t virtualPINE;
#define PINE    (virtualPINE)
#define PINE6   6
#define PINE5   5
#define PINE4   4
#define PINE3   3
#define PINE2   2
#define PINE1   1
#define PINE0   0

extern volatile uint8_t virtualDDRE;
#define DDRE    (virtualDDRE)
#define DDRE6   6
#define DDRE5   5
#define DDRE4   4
#define DDRE3   3
#define DDRE2   2
#define DDRE1   1
#define DDRE0   0

extern volatile uint8_t virtualPORTE;
#define PORTE   (virtualPORTE)
#define PORTE6  6
#define PORTE5  5
#define PORTE4  4
#define PORTE3  3
#define PORTE2  2
#define PORTE1  1
#define PORTE0  0

// TWI
extern volatile uint8_t virtualTWBR0;
#define TWBR0   (virtualTWBR0)

extern volatile uint8_t virtualTWSR0;
#define TWSR0   (virtualTWSR0)
#define TWPS0   0
#define TWPS1   1
#define TWS03   3
#define TWS04   4
#define TWS05   5
#define TWS06   6
#define TWS07   7

extern volatile uint8_t virtualTWAR0;
#define TWAR0   (virtualTWAR0)
#define TWGCE   0
#define TWA0    1
#define TWA1    2
#define TWA2    3
#define TWA3    4
#define TWA4    5
#define TWA5    6
#define TWA6    7

extern volatile uint8_t virtualTWDR0;
#define TWDR0   (virtualTWDR0)

extern volatile uint8_t virtualTWCR0;
#define TWCR0   (virtualTWCR0)
#define TWIE    0
#define TWEN    2
#define TWWC    3
#define TWSTO   4
#define TWSTA   5
#define TWEA    6
#define TWINT   7

extern volatile uint8_t virtualTWAMR0;
#define TWAMR0  (virtualTWAMR0)
#define TWAM00  1
#define TWAM01  2
#define TWAM02  3
#define TWAM03  4
#define TWAM04  5
#define TWAM05  6
#define TWAM06  7

extern volatile uint8_t virtualTWBR1;
#define TWBR1   (virtualTWBR1)

extern volatile uint8_t virtualTWSR1;
#define TWSR1   (virtualTWSR1)

extern volatile uint8_t virtualTWAR1;
#define TWAR1   (virtualTWAR1)

extern volatile uint8_t virtualTWDR1;
#define TWDR1   (virtualTWDR1)

extern volatile uint8_t virtualTWCR1;
#define TWCR1   (virtualTWCR1)

extern volatile uint8_t virtualTWAMR1;
#define TWAMR1  (virtualTWAMR1)
#define TWAM10  1
#define TWAM11  2
#define TWAM12  3
#define TWAM13  4
#define TWAM14  5
#define TWAM15  6
#define TWAM16  7

extern volatile uint8_t virtualADCL;
#define ADCL    (virtualADCL)
extern volatile uint8_t virtualADCH;
#define ADCH    (virtualADCH)

extern volatile uint8_t virtualADCSRA;
#define ADCSRA  (virtualADCSRA)
#define ADPS0   0
#define ADPS1   1
#define ADPS2   2
#define ADIE    3
#define ADIF    4
#define ADATE   5
#define ADSC    6
#define ADEN    7

extern volatile uint8_t virtualADCSRB;
#define ADCSRB  (virtualADCSRB)
#define ADTS0   0
#define ADTS1   1
#define ADTS2   2
#define ACME    6
#define GPIOEN  7

extern volatile uint8_t virtualADMUX;
#define ADMUX   (virtualADMUX)
#define MUX0    0
#define MUX1    1
#define MUX2    2
#define MUX3    3
#define MUX4    4
#define ADLAR   5
#define REFS0   6
#define REFS1   7

// USART
extern volatile uint8_t UBRR0H;
extern volatile uint8_t UBRR0L;
extern volatile uint8_t UBRR1H;
extern volatile uint8_t UBRR1L;
extern volatile uint8_t UBRR2H;
extern volatile uint8_t UBRR2L;

extern volatile uint8_t UCSR0A;
extern volatile uint8_t UCSR1A;
extern volatile uint8_t UCSR2A;

extern volatile uint8_t UCSR0B;
extern volatile uint8_t UCSR1B;
extern volatile uint8_t UCSR2B;

extern volatile uint8_t UCSR0C;
extern volatile uint8_t UCSR1C;
extern volatile uint8_t UCSR2C;

extern volatile uint8_t UCSR0D;
extern volatile uint8_t UCSR1D;
extern volatile uint8_t UCSR2D;

extern volatile uint8_t UDR0;
extern volatile uint8_t UDR1;
extern volatile uint8_t UDR2;

#define MPCM    0
#define U2X     1
#define UPE     2
#define DOR     3
#define FE      4
#define UDRE    5
#define TXC     6
#define RXC     7

#define TXB8    0
#define RXB8    1
#define UCSZ2   2
#define TXEN    3
#define RXEN    4
#define UDRIE   5
#define TXCIE   6
#define RXCIE   7

#define UCPOL   0
#define UCSZ0   1
#define UCSZ1   2
#define USBS    3
#define UPM0    4
#define UPM1    5
#define UMSEL0  6
#define UMSEL1  7

extern volatile uint8_t virtualTCCR0A;
#define TCCR0A  (virtualTCCR0A)
#define WGM00   0
#define WGM01   1
#define COM0B0  4
#define COM0B1  5
#define COM0A0  6
#define COM0A1  7

extern volatile uint8_t virtualTCCR0B;
#define TCCR0B  (virtualTCCR0B)
#define CS00    0
#define CS01    1
#define CS02    2
#define WGM02   3
#define FOC0B   6
#define FOC0A   7

extern volatile uint8_t virtualTCCR1A;
#define TCCR1A  (virtualTCCR1A)
#define WGM10   0
#define WGM11   1
#define COM1B0  4
#define COM1B1  5
#define COM1A0  6
#define COM1A1  7

extern volatile uint8_t virtualTCCR1B;
#define TCCR1B  (virtualTCCR1B)
#define CS10    0
#define CS11    1
#define CS12    2
#define WGM12   3
#define WGM13   4
#define ICES1   6
#define ICNC1   7

extern volatile uint8_t virtualTCCR2A;
#define TCCR2A  (virtualTCCR2A)
#define WGM20   0
#define WGM21   1
#define COM2B0  4
#define COM2B1  5
#define COM2A0  6
#define COM2A1  7

extern volatile uint8_t virtualTCCR2B;
#define TCCR2B  (virtualTCCR2B)
#define CS20    0
#define CS21    1
#define CS22    2
#define WGM22   3
#define FOC2B   6
#define FOC2A   7

extern volatile uint8_t virtualTCCR3A;
#define TCCR3A  (virtualTCCR3A)
#define WGM30   0
#define WGM31   1
#define COM3B0  4
#define COM3B1  5
#define COM3A0  6
#define COM3A1  7

extern volatile uint8_t virtualTCCR3B;
#define TCCR3B  (virtualTCCR3B)
#define CS30    0
#define CS31    1
#define CS32    2
#define WGM32   3
#define WGM33   4
#define ICES3   6
#define ICNC3   7

extern volatile uint8_t virtualTCCR3C;
#define TCCR3C  (virtualTCCR3C)
#define FOC3B   6
#define FOC3A   7

extern volatile uint8_t virtualTCCR4A;
#define TCCR4A  (virtualTCCR4A)
#define WGM40   0
#define WGM41   1
#define COM4B0  4
#define COM4B1  5
#define COM4A0  6
#define COM4A1  7

extern volatile uint8_t virtualTCCR4B;
#define TCCR4B  (virtualTCCR4B)
#define CS40    0
#define CS41    1
#define CS42    2
#define WGM42   3
#define WGM43   4
#define ICES4   6
#define ICNC4   7

extern volatile uint8_t virtualTCCR4C;
#define TCCR4C  (virtualTCCR4C)
#define FOC4B   6
#define FOC4A   7

extern volatile uint16_t virtualOCR1A;
#define OCR1A   (virtualOCR1A)

extern volatile uint8_t virtualOCR1AL;
#define OCR1AL  (virtualOCR1AL)

extern volatile uint8_t virtualOCR1AH;
#define OCR1AH  (virtualOCR1AH)

extern volatile uint8_t virtualTCNT0;
#define TCNT0   (virtualTCNT0)

extern volatile uint8_t virtualOCR0A;
#define OCR0A   (virtualOCR0A)

extern volatile uint8_t virtualOCR0B;
#define OCR0B   (virtualOCR0B)

extern volatile uint16_t virtualICR1;
#define ICR1    (virtualICR1)

extern volatile uint8_t virtualICR1L;
#define ICR1L   (virtualICR1L)

extern volatile uint8_t virtualICR1H;
#define ICR1H   (virtualICR1H)

extern volatile uint8_t virtualTCNT2;
#define TCNT2   (virtualTCNT2)

extern volatile uint8_t virtualOCR2A;
#define OCR2A   (virtualOCR2A)

extern volatile uint8_t virtualOCR2B;
#define OCR2B   (virtualOCR2B)

extern volatile uint16_t virtualICR3;
#define ICR3    (virtualICR3)

extern volatile uint8_t virtualICR3L;
#define ICR3L   (virtualICR3L)

extern volatile uint8_t virtualICR3H;
#define ICR3H   (virtualICR3H)

extern volatile uint16_t virtualOCR3A;
#define OCR3A   (virtualOCR3A)

extern volatile uint8_t virtualOCR3AL;
#define OCR3AL  (virtualOCR3AL)

extern volatile uint8_t virtualOCR3AH;
#define OCR3AH  (virtualOCR3AH)

extern volatile uint16_t virtualOCR3B;
#define OCR3B   (virtualOCR3B)

extern volatile uint8_t virtualOCR3BL;
#define OCR3BL  (virtualOCR3BL)

extern volatile uint8_t virtualOCR3BH;
#define OCR3BH  (virtualOCR3BH)

extern volatile uint16_t virtualICR4;
#define ICR4    (virtualICR4)

extern volatile uint8_t virtualICR4L;
#define ICR4L   (virtualICR4L)

extern volatile uint8_t virtualICR4H;
#define ICR4H   (virtualICR4H)

extern volatile uint16_t virtualOCR4A;
#define OCR4A   (virtualOCR4A)

extern volatile uint8_t virtualOCR4AL;
#define OCR4AL  (virtualOCR4AL)

extern volatile uint8_t virtualOCR4AH;
#define OCR4AH  (virtualOCR4AH)

extern volatile uint16_t virtualOCR4B;
#define OCR4B   (virtualOCR4B)

extern volatile uint8_t virtualOCR4BL;
#define OCR4BL  (virtualOCR4BL)

extern volatile uint8_t virtualOCR4BH;
#define OCR4BH  (virtualOCR4BH)

extern volatile uint16_t virtualUBRR0;
#define UBRR0   (virtualUBRR0)

extern volatile uint16_t virtualUBRR1;
#define UBRR1   (virtualUBRR1)

extern volatile uint16_t virtualUBRR2;
#define UBRR2   (virtualUBRR2)

#endif //_AVR_ATMEGA324PB_H_INCLUDED
