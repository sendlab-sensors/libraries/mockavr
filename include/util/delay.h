#ifndef _UTIL_DELAY_H_
#define _UTIL_DELAY_H_

void _delay_ms(double ms);

void _delay_us(double us);

#endif