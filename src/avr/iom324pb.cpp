#if defined(__AVR_ATmega324PB__)
    #include "avr/iom324pb.h"

    volatile uint8_t virtualPINA = 0;
    volatile uint8_t virtualDDRA = 0;
    volatile uint8_t virtualPORTA = 0;
    volatile uint8_t virtualPINB = 0;
    volatile uint8_t virtualDDRB = 0;
    volatile uint8_t virtualPORTB = 0;
    volatile uint8_t virtualPINC = 0;
    volatile uint8_t virtualDDRC = 0;
    volatile uint8_t virtualPORTC = 0;
    volatile uint8_t virtualPIND = 0;
    volatile uint8_t virtualDDRD = 0;
    volatile uint8_t virtualPORTD = 0;
    volatile uint8_t virtualPINE = 0;
    volatile uint8_t virtualDDRE = 0;
    volatile uint8_t virtualPORTE = 0;

    volatile uint8_t virtualTWBR0 = 0;
    volatile uint8_t virtualTWSR0 = 0;
    volatile uint8_t virtualTWAR0 = 0;
    volatile uint8_t virtualTWDR0 = 0;
    volatile uint8_t virtualTWCR0 = 0;
    volatile uint8_t virtualTWAMR0 = 0;
    volatile uint8_t virtualTWBR1 = 0;
    volatile uint8_t virtualTWSR1 = 0;
    volatile uint8_t virtualTWAR1 = 0;
    volatile uint8_t virtualTWDR1 = 0;
    volatile uint8_t virtualTWCR1 = 0;
    volatile uint8_t virtualTWAMR1 = 0;

    volatile uint8_t virtualADCL = 0;
    volatile uint8_t virtualADCH = 0;
    volatile uint8_t virtualADCSRA = 0;
    volatile uint8_t virtualADCSRB = 0;
    volatile uint8_t virtualADMUX = 0;


    volatile uint8_t UBRR0H = 0;
    volatile uint8_t UBRR0L = 0;
    volatile uint8_t UBRR1H = 0;
    volatile uint8_t UBRR1L = 0;
    volatile uint8_t UBRR2H = 0;
    volatile uint8_t UBRR2L = 0;

    volatile uint8_t UCSR0A = 0;
    volatile uint8_t UCSR1A = 0;
    volatile uint8_t UCSR2A = 0;

    volatile uint8_t UCSR0B = 0;
    volatile uint8_t UCSR1B = 0;
    volatile uint8_t UCSR2B = 0;

    volatile uint8_t UCSR0C = 0;
    volatile uint8_t UCSR1C = 0;
    volatile uint8_t UCSR2C = 0;

    volatile uint8_t UCSR0D = 0;
    volatile uint8_t UCSR1D = 0;
    volatile uint8_t UCSR2D = 0;

    volatile uint8_t UDR0 = 0;
    volatile uint8_t UDR1 = 0;
    volatile uint8_t UDR2 = 0;

    volatile uint8_t virtualTCCR0A = 0;
    volatile uint8_t virtualTCCR0B = 0;
    volatile uint8_t virtualTCCR1A = 0;
    volatile uint8_t virtualTCCR1B = 0;
    volatile uint8_t virtualTCCR2A = 0;
    volatile uint8_t virtualTCCR2B = 0;
    volatile uint8_t virtualTCCR3A = 0;
    volatile uint8_t virtualTCCR3B = 0;
    volatile uint8_t virtualTCCR3C = 0;
    volatile uint8_t virtualTCCR4A = 0;
    volatile uint8_t virtualTCCR4B = 0;
    volatile uint8_t virtualTCCR4C = 0;
    volatile uint16_t virtualOCR1A = 0;
    volatile uint8_t virtualOCR1AL = 0;
    volatile uint8_t virtualOCR1AH = 0;
    volatile uint8_t virtualTCNT0 = 0;
    volatile uint8_t virtualOCR0A = 0;
    volatile uint8_t virtualOCR0B = 0;
    volatile uint16_t virtualICR1 = 0;
    volatile uint8_t virtualICR1L = 0;
    volatile uint8_t virtualICR1H = 0;
    volatile uint8_t virtualTCNT2 = 0;
    volatile uint8_t virtualOCR2A = 0;
    volatile uint8_t virtualOCR2B = 0;
    volatile uint16_t virtualICR3 = 0;
    volatile uint8_t virtualICR3L = 0;
    volatile uint8_t virtualICR3H = 0;
    volatile uint16_t virtualOCR3A = 0;
    volatile uint8_t virtualOCR3AL = 0;
    volatile uint8_t virtualOCR3AH = 0;
    volatile uint16_t virtualOCR3B = 0;
    volatile uint8_t virtualOCR3BL = 0;
    volatile uint8_t virtualOCR3BH = 0;
    volatile uint16_t virtualICR4 = 0;
    volatile uint8_t virtualICR4L = 0;
    volatile uint8_t virtualICR4H = 0;
    volatile uint16_t virtualOCR4A = 0;
    volatile uint8_t virtualOCR4AL = 0;
    volatile uint8_t virtualOCR4AH = 0;
    volatile uint16_t virtualOCR4B = 0;
    volatile uint8_t virtualOCR4BL = 0;
    volatile uint8_t virtualOCR4BH = 0;
    volatile uint16_t virtualUBRR0 = 0;
    volatile uint16_t virtualUBRR1 = 0;
    volatile uint16_t virtualUBRR2 = 0;
#endif